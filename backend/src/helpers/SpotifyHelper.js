const SpotifyWebApi = require('spotify-web-api-node')
const config = require('../config')
const { clientId, clientSecret, redirectUri } = config.spotify
const { User } = require('../models')
const moment = require('moment')

module.exports = {
    getAPIObject(accessToken, refreshToken) {
        return new SpotifyWebApi({
            clientId,
            clientSecret,
            redirectUri,
            accessToken,
            refreshToken
        })
    },
    handleError(req, res, err) {
        // access token is no longer valid
        const { status } = err.body.error
        const tokenExpired = moment().isAfter(moment(req.user.TokenExpires))
        const shouldTryRefresh = req.user.AccessToken && req.user.RefreshToken

        if(status === 401 && tokenExpired && shouldTryRefresh) {
            console.log("refreshing token", req.user)
            // refresh token
            return this.getAPIObject(req.user.AccessToken, req.user.RefreshToken).refreshAccessToken().then(async data => {
                const accessToken = data.body['access_token']
                const expiresIn = data.body['expires_in']

                // update User model
                const user = await User.findByPk(req.user.ID)
                user.AccessToken = accessToken
                user.TokenExpires = moment.utc().add(expiresIn, 'seconds')
                user.save()

                // update req.user
                req.user = user.toJSON()

                return true
            }).catch(async error => {
                // refresh not possible - user has to login manually
                console.error(error)
                const user = await User.findByPk(req.user.ID)
                user.AccessToken = user.RefreshToken = null
                user.save()

                // update req.user
                req.user = user.toJSON()
                console.log("updated user, handled error")

                return false
            })
        } else {
            console.log('Something went wrong!', err)
            console.log(tokenExpired ? 'Token expired.' : 'Token is valid.')

            res.status(401).send()
            return false
        }
    }
}

const jwt = require('jsonwebtoken')
const config = require('../config')
const { User, Session } = require('../models')

module.exports = {
    async isUserAuthenticated (token) {
        return jwt.verify(token, config.auth.jwtSecret, async (error, decoded) => {
            if(error) {
                // unauthorized
                return false
            } else {
                // get user from database
                const session = await Session.findByPk(decoded.SessionID)
                const user = await User.findByPk(decoded.ID, { raw: true })

                // additionally check if spotify access and refresh token exist
                if(session && user && user.AccessToken && user.RefreshToken) {
                    return user
                } else {
                    // session expired or user no longer in database
                    return false
                }
            }
        })
    }
}

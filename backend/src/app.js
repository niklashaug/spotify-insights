const express = require('express')
const cookieParser = require('cookie-parser')
const app = express()
const port = 3000
const history = require('connect-history-api-fallback')
const { sequelize } = require('./models')
const winston = require('winston')
require('winston-daily-rotate-file')

if (process.env.NODE_ENV === 'production') {
    const transport = new winston.transports.DailyRotateFile({
        filename: '%DATE%.log',
        dirname: 'logs'
    })

    const logger = winston.createLogger({
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json()
        ),
        defaultMeta: { service: 'user-service' },
        transports: [
            transport
        ]
    })

    console.log = (...args) => logger.info.call(logger, ...args)
    console.info = (...args) => logger.info.call(logger, ...args)
    console.warn = (...args) => logger.warn.call(logger, ...args)
    console.error = (...args) => logger.error.call(logger, ...args)
    console.debug = (...args) => logger.debug.call(logger, ...args)
}

sequelize.sync()

app.use(cookieParser())

// API Routes
const router = express.Router()
require('./routes')(app, router)

app.use(history())

app.use(express.static(__dirname + '/dist'))

app.listen(port, () => {
    console.log(`app is listening on port ${port}`)
})

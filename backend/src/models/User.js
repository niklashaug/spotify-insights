module.exports = (sequelize, DataTypes) =>
    sequelize.define('User', {
        ID: {
            primaryKey: true,
            allowNull: false,
            type: DataTypes.STRING
        },
        Email: {
            allowNull: false,
            type: DataTypes.STRING
        },
        AccessToken: {
            allowNull: true,
            type: DataTypes.STRING
        },
        RefreshToken: {
            allowNull: true,
            type: DataTypes.STRING
        },
        TokenExpires: {
            allowNull: false,
            type: DataTypes.DATE
        }
    })

module.exports = (sequelize, DataTypes) =>
    sequelize.define('Session', {
        ID: {
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
            type: DataTypes.INTEGER
        },
        UserID: {
            allowNull: false,
            type: DataTypes.STRING,
            references: {
                model: 'Users',
                key: 'ID'
            }
        },
        Expires: {
            allowNull: false,
            type: DataTypes.DATE
        }
    })

const fs = require('fs')
const path = require('path')
const Sequelize	= require('sequelize')
const config = require('../config')
const db = {}

// configure database connection
const sequelize = new Sequelize(
    config.db.name,
    config.db.user,
    config.db.password,
    config.db.options
)

// test database connection
sequelize
    .authenticate()
    .then(() => {
        console.log(`${config.db.options.dialect}: connected`)
    })
    .catch(err => {
        console.log(`${config.db.options.dialect}: unable to connect:`, err)
    })

// read all models out of the current directory
fs
    .readdirSync(__dirname)
    .filter(file =>
        file !== 'index.js'
    )
    .forEach(file => {
        const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes)
        db[model.name] = model
    })

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db

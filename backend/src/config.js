require('dotenv').config()

module.exports = {
    spotify: {
        clientId: process.env.SPOTIFY_CLIENT_ID,
        clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
        scopes: ['user-read-private', 'user-read-email', 'user-library-read', 'user-library-modify', 'user-read-currently-playing', 'user-read-recently-played', 'user-read-playback-state', 'user-top-read'],
        redirectUri: process.env.SPOTIFY_REDIRECT_URI,
        state: 'generated-csrf-token'
    },
    db: {
        name: process.env.DB_NAME || 'spotify-insights',
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        options: {
            dialect: process.env.DB_DIALECT || 'mysql',
            host: process.env.DB_HOST || 'localhost',
            port: process.env.DB_PORT || 3306
        }
    },
    auth: {
        redirectURL: process.env.AUTH_REDIRECT || 'http://localhost:8080/dashboard',
        jwtSecret: process.env.JWT_SECRET
    }
}

const AuthHelper = require('../helpers/AuthHelper')

module.exports = {
    async checkUser (req, res, next) {
        const user = await AuthHelper.isUserAuthenticated(req.cookies['Authentication'])
        if(user) {
            req.user = user
            next()
        } else {
            res.status(401).send()
        }
    }
}

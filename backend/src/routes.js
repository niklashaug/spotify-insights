const AuthController = require('./controllers/AuthController')
const SpotifyController = require('./controllers/SpotifyController')

const AuthPolicy = require('./policies/AuthPolicy')

module.exports = (app, router) => {
    // INDEX
    router.get('/', (req, res) => {
        res.send("spotify-insights api v1.0")
    })

    // TRACKS
    router.get('/tracks/top', AuthPolicy.checkUser, SpotifyController.getTopTracks)
    router.get('/tracks/recent', AuthPolicy.checkUser, SpotifyController.getRecentTracks)
    router.get('/tracks/current', AuthPolicy.checkUser, SpotifyController.getCurrentTrack)
    router.post('/tracks/:trackID', AuthPolicy.checkUser, SpotifyController.saveTrack)
    router.delete('/tracks/:trackID', AuthPolicy.checkUser, SpotifyController.deleteTrack)
    router.get('/tracks/:trackID/status', AuthPolicy.checkUser, SpotifyController.getTrackStatus)

    // ARTISTS
    router.get('/artists/top', AuthPolicy.checkUser, SpotifyController.getTopArtists)

    // PROFILE
    router.get('/profile', AuthPolicy.checkUser, AuthController.getProfile)

    // AUTH
    router.get('/auth/callback', AuthController.handleCallback)
    router.get('/auth/status', AuthController.getAuthStatus)
    router.post('/auth/logout', AuthPolicy.checkUser, AuthController.logout)

    app.use('/api', router)
}

const config = require('../config')
const { scopes, state } = config.spotify
const SpotifyHelper = require('../helpers/SpotifyHelper')
const AuthHelper = require('../helpers/AuthHelper')
const { User, Session } = require('../models')
const jwt = require('jsonwebtoken')
const moment = require('moment')

const self = module.exports = {
    handleCallback(req, res) {
        const spotify = SpotifyHelper.getAPIObject()
        const code = req.query.code

        spotify.authorizationCodeGrant(code).then(async data => {
            const accessToken = data.body['access_token'],
                refreshToken = data.body['refresh_token'],
                expiresIn = data.body['expires_in']

            console.log(`The token expires in ${expiresIn / 60} minutes.`)
            console.log('The access token is ' + accessToken)
            console.log('The refresh token is ' + refreshToken)

            const userData = (await SpotifyHelper.getAPIObject(accessToken).getMe()).body

            const user = await User.findOne({
                where: {
                    ID: userData.id
                }
            })

            if(user) {
                // update token of existing user
                user.AccessToken = accessToken
                user.RefreshToken = refreshToken
                user.TokenExpires = moment.utc().add(expiresIn, 'seconds')
                user.save()
            } else {
                await User.create({
                    ID: userData.id,
                    Email: userData.email,
                    AccessToken: accessToken,
                    RefreshToken: refreshToken,
                    TokenExpires: moment.utc().add(expiresIn, 'seconds')
                })
            }

            const session = await Session.create({
                UserID: userData.id,
                Expires: moment.utc().add(1, 'year')
            })

            // todo: add signed jwt token containing user id
            const token = jwt.sign({
                ID: userData.id,
                SessionID: session.ID
            }, config.auth.jwtSecret, {
                expiresIn: '1y'
            })

            // todo: add more cookie options
            res.cookie('Authentication', token, { maxAge: 365 * 24 * 60 * 60 * 1000, httpOnly: true })
            res.redirect(config.auth.redirectURL)
        })
    },
    async getAuthStatus (req, res) {
        const authenticated = !! await AuthHelper.isUserAuthenticated(req.cookies['Authentication'])

        res.send({
            authenticated,
            authURL: SpotifyHelper.getAPIObject().createAuthorizeURL(scopes, state)
        })
    },
    async getProfile(req, res) {
        try {
            const profile = (await SpotifyHelper.getAPIObject(req.user.AccessToken).getMe()).body

            res.send(profile)
        } catch (err) {
            if(SpotifyHelper.handleError(req, res, err)) {
                self.getProfile(req, res)
            }
        }
    },
    async logout(req, res) {
        await Session.destroy({
            where: {
                UserID: req.user.ID
            }
        })

        res.send({
            success: true
        })
    }
}

const SpotifyHelper = require('../helpers/SpotifyHelper')

const self = module.exports = {
    async getCurrentTrack(req, res) {
        try {
            const track = (await SpotifyHelper.getAPIObject(req.user.AccessToken).getMyCurrentPlayingTrack()).body.item
            const isTrackInLibrary = track ? (await SpotifyHelper.getAPIObject(req.user.AccessToken).containsMySavedTracks([track.id])).body[0] : null

            res.send({
                ...track,
                isTrackInLibrary
            })
        } catch (err) {
            if(SpotifyHelper.handleError(req, res, err)) {
                self.getCurrentTrack(req, res)
            }
        }
    },
    async saveTrack(req, res) {
        try {
            await SpotifyHelper.getAPIObject(req.user.AccessToken).addToMySavedTracks([req.params.trackID])

            res.send({
                success: true
            })
        } catch (err) {
            if(SpotifyHelper.handleError(req, res, err)) {
                self.saveTrack(req, res)
            }
        }
    },
    async deleteTrack(req, res) {
        try {
            await SpotifyHelper.getAPIObject(req.user.AccessToken).removeFromMySavedTracks([req.params.trackID])

            res.send({
                success: true
            })
        } catch (err) {
            if(SpotifyHelper.handleError(req, res, err)) {
                self.deleteTrack(req, res)
            }
        }
    },
    async getTrackStatus(req, res) {
        try {
            const isTrackInLibrary = (await SpotifyHelper.getAPIObject(req.user.AccessToken).containsMySavedTracks([req.params.trackID])).body[0]
            res.send({
                isTrackInLibrary
            })
        } catch (err) {
            if(SpotifyHelper.handleError(req, res, err)) {
                self.getTrackStatus(req, res)
            }
        }
    },
    async getTopTracks(req, res) {
        try {
            const tracks = (await SpotifyHelper.getAPIObject(req.user.AccessToken).getMyTopTracks()).body.items

            res.send(tracks)
        } catch (err) {
            if(SpotifyHelper.handleError(req, res, err)) {
                self.getTopTracks(req, res)
            }
        }
    },
    async getTopArtists (req, res) {
        try {
            const artists = (await SpotifyHelper.getAPIObject(req.user.AccessToken).getMyTopArtists()).body.items

            res.send(artists)
        } catch(err) {
            if(SpotifyHelper.handleError(req, res, err)) {
                self.getTopArtists(req, res)
            }
        }
    },
    async getRecentTracks(req, res) {
        try {
            const tracks = (await SpotifyHelper.getAPIObject(req.user.AccessToken).getMyRecentlyPlayedTracks({
                limit : 20
            })).body.items.map(item => item.track)

            res.send(tracks)
        } catch (err) {
            if(SpotifyHelper.handleError(req, res, err)) {
                self.getRecentTracks(req, res)
            }
        }
    }
}

module.exports = {
    purge: {
        enabled: process.env.NODE_ENV === 'production',
        content: [
            './public/**/*.html',
            './src/**/*.vue',
            './src/*.vue'
        ]
    },
    darkMode: 'media', // or 'media' or 'class'
    theme: {
        extend: {}
    },
    variants: {
        extend: {}
    },
    plugins: [
        require('daisyui'),
        require('@tailwindcss/typography')
    ]
}

module.exports = {
    ...(process.env.NODE_ENV === 'production' && {
        publicPath: process.env.VUE_APP_PUBLIC_PATH
    }),
    devServer: {
        proxy: 'http://localhost:3000'
    }
}

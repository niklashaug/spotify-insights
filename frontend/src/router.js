import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '@/views/Dashboard.vue'
import Home from '@/views/Home'
import TopTracks from '@/views/TopTracks'
import RecentTracks from '@/views/RecentTracks'
import TopArtists from '@/views/TopArtists'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard
    },
    {
        path: '/top-tracks',
        name: 'topTracks',
        component: TopTracks
    },
    {
        path: '/top-artists',
        name: 'topArtists',
        component: TopArtists
    },
    {
        path: '/recent-tracks',
        name: 'recentTracks',
        component: RecentTracks
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router

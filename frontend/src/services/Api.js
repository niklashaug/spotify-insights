import axios from 'axios'
import store from '@/store'
import router from '@/router'

export default () => {
    const api = axios.create({
        baseURL: '/api'
    })

    api.interceptors.response.use(undefined, error => {
        if (error.response?.status === 401 && router.currentRoute.name !== 'home') {
            store.commit('setProfile', null)
            router.push({ name: 'home' })
        }
        throw error
    })

    return api
}

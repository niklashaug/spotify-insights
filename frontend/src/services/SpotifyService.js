import Api from '@/services/Api'

export default {
    getRecentTracks () {
        return Api().get('/tracks/recent')
    },
    getTopTracks () {
        return Api().get('/tracks/top')
    },
    getTopArtists () {
        return Api().get('/artists/top')
    },
    getCurrentTrack () {
        return Api().get('/tracks/current')
    },
    getProfileData () {
        return Api().get('/profile')
    },
    addTrackToLibrary (trackID) {
        return Api().post(`/tracks/${trackID}`)
    },
    removeTrackFromLibrary (trackID) {
        return Api().delete(`/tracks/${trackID}`)
    },
    isTrackInLibrary (trackID) {
        return Api().get(`/tracks/${trackID}/status`)
    }
}

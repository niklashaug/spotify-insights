import Api from '@/services/Api'

export default {
    getAuthStatus () {
        return Api().get('/auth/status')
    },
    logout () {
        return Api().post('/auth/logout')
    }
}
